/****************************************************************************
**
** Copyright (c) 2016, Leon Manukyan
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
**
** 1. Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import OSG 1.0

Item {
    width: 400
    height: 400

    OSGItem {
        id: osgitem
        anchors.fill: parent
        focus: true
    }

    OSGModelLoader {
        id: loader
        target: osgitem
    }

    Rectangle {
        id: labelFrame
        anchors.margins: -10
        radius: 5
        color: "white"
        border.color: "black"
        opacity: 0.8
        anchors.fill: label
    }

    Text {
        id: label
        anchors.bottom: osgitem.bottom
        anchors.left: osgitem.left
        anchors.right: osgitem.right
        anchors.margins: 20
        wrapMode: Text.WordWrap
        text: "You can manipulate the scene view with the mouse."
    }

    Button {
        id: openButton
        anchors.top: osgitem.top
        anchors.left: osgitem.left
        anchors.margins: 20
        text: "Open"
        onClicked: fileDialog.open()
    }

    //! [filedialog]
    FileDialog {
        id: fileDialog
        visible: false
        modality: Qt.WindowModal
        title: "Choose a file"
        selectExisting: true
        selectMultiple: false
        selectFolder: false
        nameFilters: [ "All image files (*.osg *.ply)",
            "OSG files (*.osg)",
            "PLY files (*.ply)"
        ]
        selectedNameFilter: "All image files (*.osg *.ply)"
        sidebarVisible: true
        onAccepted: {
            loader.modelPath = fileDialog.fileUrl
        }
    }
}
